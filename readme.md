This is an example-project to demonstrate how to request an additional SAML-Authentication 
using Resolution's SAML SingleSignOn-Plugin-

### How to build this example-code
The Atlassian SDK is reqired. Checkout the project and build it using
`atlas-mvn clean verify`

You can also build and install it on a test-system using
`atlas-mvn clean verify -Dupm.user=<user> -Dupm.password=<password> -Dupm.host=https://<testhost>`

open `https://<instance>/plugins/servlet/samlauth` to test the REST-based implementation 
or `https://<instance>/plugins/servlet/samlauth-backend` for the Java-API-based implementation.

## Use the API

### REST-based implementation (recommended)

To test if additional authentication is available for the current session, send a GET-request to `https://<baseurl>/rest/samlsso-admin/1.0/additionalauth/enabled`.
This should result in a 404-response if SAML SingleSignOn is not installed.

If the currently logged in user has authenticated using SAML SSO and additional authentication has been enabled in the
SAML-Plugin-configuration a JSON-result  like this is returned:
```json
{"enabled":true}
```

To request an additional SAML-authentication, send a POST-request to `https://<baseurl>/rest/samlsso-admin/1.0/additionalauth/new` 
with content-type Application/JSON and a body like this:
````json
{
    "confirmationMessage": "MessageToShow"
}
````
The confirmationMesssage is shown in the confirmation dialog displayed after the user has successfully authenticated
at the SAML Identity Provider. It should refer to the action associated with this authentication.

The reponse to this POST-request should look like this:
````json
{
    "statusId":"RLNVS",
    "authenticationUrl":"/plugins/servlet/samlsso?additional&tracker=RLNVS",
    "statusUrl":"/rest/samlsso-admin/1.0/additionalauth/status/RLNVS",
    "enabled":true
}
````

If the currently logged in user did not login using SAML, the JSON just contains `"enabled":false`.

The `authenticationUrl` should be opened in a new window using JavaScript (this allows the window to close automatically after authentication), e.g.
```javascript
window.open(result.authenticationUrl,'newwindow','width=600,height=600');
```

Calling `statusUrl` returns a JSON-result like this:

```json
{
  "success":true,
  "confirmed":true,
  "statusMessage":"Reauthentication was successful",
  "done":true,
  "id":"MTJWQ",
  "lastModified":1592936277279
}
```

If `done` is true, `success` indicates that the authentication was sucessful. `confirmed` indicates that the user has
acknowledged the action after successful authentication. **Both success and confirmed should be checked to be true.**

If `success` false, the authentication has failed and `statusMessage` should contain an error message.
If the authentication fails on the IdP, `done` will never set to true in most cases because the SAML IdP does not send a response. 
Therefore, a reasonable timeout should be applied. 

After the SAML-Authentication, the attribute `additionalSAMLAuthentication` in the browser's local storage is updated 
with the event-key `additionalSAMLAuthentication`. The value stored has now relevance, but adding an event listener 
to `storage` can be used to trigger loading the status instead of polling periodically:
```javascript
window.addEventListener('storage', function(event) {
    if(event.key === 'additionalSAMLAuthentication') {
        readStatus(result.statusUrl,maxRetry); 
    }
}
```


### Java-API
JAVA-API is not documented yet.
Check `/src/main/resources/reference-backend.js` and `/src/main/jave/de/resolution/samlauth/reference/BackendBasedSAMLAuthReference.java` for example code

To use the Java-API, add these repositories to pom.xml:
```xml
<repositories>
  <repository>
    <id>resolution.public.release</id>
    <url>http://public.maven.resolution.de/release</url>
    </repository>
    <repository>
       <id>resolution.public.snapshot</id>
       <url>http://public.maven.resolution.de/snapshot</url>
  </repository>
</repositories>
```

and add this dependency:
```xml
 <dependency>
   <groupId>de.resolution</groupId>
   <artifactId>samlsso-additionalauth</artifactId>
   <version>0.1.0</version>
   <scope>provided</scope>
 </dependency>
```

## Limitations
Requesting a new SAML-authentication is available only if the currently logged in user has already authenticated using SAML and 
the SAML Identity-provider must return the same Name-ID as in the initial authentication. That means
- The additional authentication must return the same user as the initial authentication- it is not possible to login as another user.
- Transient Name-IDs are not supported
