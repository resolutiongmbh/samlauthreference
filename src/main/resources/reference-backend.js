'use strict';

(function () {
    window.AJS.toInit(function () {
        var authenticationUrl = AJS.$('meta[name=authenticationUrl]').attr("content");
        var afterAuthenticationUrl =  AJS.$('meta[name=afterAuthenticationUrl]').attr("content");
        var awaitingAuthentication = false;
        console.log(authenticationUrl);
        console.log(afterAuthenticationUrl);
        console.log("Adding event-listener to storage");
        window.addEventListener('storage', function(event) {
            if(event.key === 'additionalSAMLAuthentication' && awaitingAuthentication) {
                console.log('Loading ' + afterAuthenticationUrl);
                awaitingAuthentication = false;
                window.location.href = afterAuthenticationUrl;
            } else {
                console.log('Ignoring local storage event with key ' + event.key);
            }
        });

        if(authenticationUrl !== '') {
            awaitingAuthentication = true;
            console.log("Opening authentication-window");
            window.open(authenticationUrl, 'newwindow', 'width=600,height=600');
        }
    });
})();
