'use strict';

(function () {

    var $authButton;
    var $authStatus;

    var authenticationTimeout = 15000; // Wait 15 seconds for the user to come back from the IdP
    var authenticationTimeoutHandle;

    var additionalAuthBase =  AJS.contextPath() + '/rest/samlsso-admin/1.0/additionalauth';
    var waitingForAuthentication = false;
    var statusUrl;

    // Shows the  Request Authentication-button
    // if the current user is logged in using SAML SSO
    var checkSaml = function() {
        window.AJS.$.ajax({
            url: additionalAuthBase + '/enabled',
            dataType: "json",
            success: function (data) {
                console.log(data);
                if(data.enabled) {
                    $authButton.show();
                    $authButton.click(triggerAuthentication);
                } else {
                    $authButton.hide();
                    $authStatus.text("This is no SAML-Session enabled for additional authentication");
                    $authStatus.removeClass();
                    $authStatus.addClass("aui-message aui-message-error");
                }
            },
            error: function(error) {
                console.log(error);
                $authButton.hide();
                if(error.status === 404) {
                    $authStatus.text('SAML SingleSignOn-plugin is not active');
                    $authStatus.removeClass();
                    $authStatus.addClass("aui-message aui-message-info");
                } else {
                    $authStatus.text('Checking for SAML-session failed, check console for details');
                    $authStatus.removeClass();
                    $authStatus.addClass("aui-message aui-message-error");
                }
            }
        });
    };

    // Opens a new window pointing to the SAML-IdP
    // The result is fetched after the localstorage has been updated with the key additionalSAMLAuthentication
    var triggerAuthentication = function() {
        window.AJS.$.ajax({
            url: additionalAuthBase + '/new',
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ "confirmationMessage": "Test for reference-frontend" }),
            success: function(result) {
                statusUrl = result.statusUrl;
                waitingForAuthentication = true;
                authenticationTimeoutHandle = window.setTimeout(showTimeout, authenticationTimeout);
                // Opens the IdP's authentication page in a new window.
                // This window is automatically closed after successful SAML-authentication
                // if it was opened using JavaScript.
                window.open(result.authenticationUrl,'newwindow','width=600,height=600');


                $authStatus.text("Waiting for authentication...");
                $authStatus.removeClass();
                $authStatus.addClass("aui-message aui-message-info");
            },
            error: function(error) {
                console.log(error);
                $authStatus.text(JSON.stringify(error));
                $authStatus.removeClass();
                $authStatus.addClass("aui-message aui-message-error");
            }
        });
    };

    var showTimeout = function() {
        waitingForAuthentication = false;
        $authStatus.text('Authentication timed out.');
        $authStatus.removeClass();
        $authStatus.addClass("aui-message aui-message-error");
    };

    // The AdditionalAuthentication-page in the SAML-Plugin will update
    // the local storage with the key additionalSAMLAuthentication with
    // a new timestamp. We don't really care vor the value here, but
    // use the storage-event as trigger to detect that the user is
    // back from the SAML-IdP and read the status
    var storageEventListener = function(event) {
        if(event.key === 'additionalSAMLAuthentication' && waitingForAuthentication) {
            waitingForAuthentication = false;
            window.clearTimeout(authenticationTimeoutHandle);
            window.AJS.$.ajax({
                url: statusUrl,
                dataType: "json",
                success: function (status) {
                    if (!status.success) {
                        console.log(status);
                        $authStatus.text(status.statusMessage);
                        $authStatus.removeClass();
                        $authStatus.addClass("aui-message aui-message-error");
                    } else {
                        if(!status.confirmed) {
                            $authStatus.text("Action was not confirmed!");
                            $authStatus.removeClass();
                            $authStatus.addClass("aui-message aui-message-error");
                        } else {
                            $authStatus.text("Authentication was successful.");
                            $authStatus.removeClass();
                            $authStatus.addClass("aui-message aui-message-success");
                        }
                    }
                },
                error: function (error) {
                    $authStatus.text(error);
                    $authStatus.removeClass();
                    $authStatus.addClass("aui-message aui-message-error");
                    console.log(error);
                }
            });
        }
    };

    window.AJS.toInit(function () {
        $authStatus = $('#auth-status');
        $authButton = $('#request-auth-button');
        window.addEventListener('storage', storageEventListener);
        checkSaml();
    });
})();
