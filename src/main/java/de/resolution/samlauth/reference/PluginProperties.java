package de.resolution.samlauth.reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Offers data from plugin.properties at runtime
 * Used to access data from this properties file which is set up during build
 * @author joergwesely
 *
 */
class PluginProperties {

	private static  final Logger logger = LoggerFactory.getLogger(PluginProperties.class);
	private static final Properties properties;

	static {
		properties = new Properties();
		InputStream is = PluginProperties.class.getClassLoader().getResourceAsStream("plugin.properties");
		if (is!=null) {
			try {
				properties.load(is);
			} catch (IOException e) {
				logger.error("Loading plugin.properties failed!",e);
			}
		}
	}

	static String get(String key) {
		return properties.getProperty(key);
	}

	private PluginProperties() {
		// Empty private constructor
	}
}
