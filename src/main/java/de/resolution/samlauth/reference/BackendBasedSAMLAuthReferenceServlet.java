package de.resolution.samlauth.reference;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import de.resolution.samlauth.api.AdditionalAuthenticationData;
import de.resolution.samlauth.api.AdditionalAuthenticationService;
import de.resolution.samlauth.api.AdditionalAuthenticationStatus;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

/**
 * Renders the test-page
 */
@Named
public class BackendBasedSAMLAuthReferenceServlet extends HttpServlet {

    private final TemplateRenderer renderer;
    private final AdditionalAuthenticationService additionalAuthenticationService;

    private final Map<String,String> authenticationRequestMap = new HashMap<>();

    private final SecureRandom secureRandom = new SecureRandom();

    @Inject
    public BackendBasedSAMLAuthReferenceServlet(
            @ComponentImport TemplateRenderer renderer,
            //TODO: Import this somehow dnyamic so that the plugin enables stays enabled independently from the SAMLSSO-Plugin
            @ComponentImport AdditionalAuthenticationService additionalAuthenticationService) {
        this.renderer = renderer;
        this.additionalAuthenticationService = additionalAuthenticationService;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        boolean samlsession = false;
        String authenticationUrl = "";
        String afterAuthenticationUrl = "";

        Map<String, Object> ctx = new HashMap<>();
        ctx.put("pluginkey", PluginProperties.get("pluginkey"));
        ctx.put("buildTimestamp", PluginProperties.get("buildTimestamp"));

        // In this case the authentication should be done and we check if it was successful
        if(request.getParameterMap().containsKey("key")) {
            String key = request.getParameter("key");
            String statusId = authenticationRequestMap.get(key);
            authenticationRequestMap.remove(key); // remove the entry to avoid a replay-attack

            final boolean success;
            final String resultMessage;
            if(statusId != null) {
                AdditionalAuthenticationStatus additionalAuthenticationStatus = additionalAuthenticationService.getStatus(statusId);
                if (additionalAuthenticationStatus != null) {
                    if(additionalAuthenticationService.isSameSession(additionalAuthenticationStatus,request)) {
                        if (additionalAuthenticationStatus.isSuccess()) {
                            if(additionalAuthenticationStatus.isConfirmed()) {
                                success = true;
                                resultMessage = "The additional authentication was successful";
                                ctx.put("resultUserid", additionalAuthenticationStatus.getUserId());
                            } else {
                                success = false;
                                resultMessage = "The additional aithentication was NOT CONFIRMED!";
                            }
                        } else {
                            success = false;
                            resultMessage = "The additional authentication was NOT successful: " + additionalAuthenticationStatus.getStatusMessage();
                        }
                    } else {
                        success = false;
                        resultMessage = "The authentication happened in a different session";
                    }
                } else {
                    success = false;
                    resultMessage = "Could not find an AdditionalAuthenticationResult for with id " + statusId;
                }
            } else {
                success = false;
                resultMessage = "Could not find an AdditionalAuthenticationResult for key " + key;
            }
            ctx.put("success",success);
            ctx.put("resultMessage",resultMessage);
            renderer.render("samlauth-backend-done.vm", ctx, response.getWriter());
        // Without the key-parameter a new authentication is triggered
        } else {
            if (additionalAuthenticationService != null) {
                AdditionalAuthenticationData additionalAuthenticationData = additionalAuthenticationService.requestAuthentication(request,"Test from reference-implementation");
                if (additionalAuthenticationData != null && additionalAuthenticationData.isEnabled()) {
                    samlsession = true;
                    authenticationUrl = additionalAuthenticationData.getAuthenticationUrl();

                    // generate a random key to identify this authentication-process when its done
                    String key = String.valueOf(secureRandom.nextInt());
                    authenticationRequestMap.put(key,additionalAuthenticationData.getStatusId());
                    afterAuthenticationUrl = "/plugins/servlet/samlauth-backend?key=" + key;
                }
            }
            ctx.put("samlSession", samlsession);
            ctx.put("authenticationUrl", authenticationUrl);
            ctx.put("afterAuthenticationUrl",afterAuthenticationUrl);
            renderer.render("samlauth-backend.vm", ctx, response.getWriter());
        }
    }
}
