package de.resolution.samlauth.reference;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Renders the test-page
 */
@Named
public class FrontendBasedSAMLAuthReferenceServlet extends HttpServlet {

    private final TemplateRenderer renderer;

    @Inject
    public FrontendBasedSAMLAuthReferenceServlet(@ComponentImport TemplateRenderer renderer) {
        this.renderer = renderer;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Object> ctx = new HashMap<>();
        ctx.put("pluginkey", PluginProperties.get("pluginkey"));
        ctx.put("buildTimestamp", PluginProperties.get("buildTimestamp"));
        renderer.render("samlauth-frontend.vm", ctx, response.getWriter());
    }
}
